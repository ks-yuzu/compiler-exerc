int PrintFizz()
{
  putchar('F');
  putchar('i');
  putchar('z');
  putchar('z');
}

int PrintBuzz()
{
  putchar('B');
  putchar('u');
  putchar('z');
  putchar('z');  
}

int main()
{
  int i;

  i = 1;
  while( i <= 100 )
  {
    if( (i % 3 == 0) * (i % 5 == 0) ) { PrintFizz(); PrintBuzz(); }
    else if( i % 3 == 0 ) { PrintFizz(); }
    else if( i % 5 == 0 ) { PrintBuzz(); }
    else                  { putint(i); }

    putchar('\n');
    i = i + 1;
  }
}

