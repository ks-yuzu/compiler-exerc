#include <iostream>
#include "ast.h"


Statement *make_if()
{
	// cond
	auto var_i = new Exp_variable("i");
	auto cst_0 = new Exp_constant(Type_INT, 0);
	auto cond = new Exp_operation2(Operator_LT, var_i, cst_0);

	// then
	auto var_s = new Exp_variable("s");
	auto diff = new Exp_operation2(Operator_MINUS, var_s, var_i);
	auto then = new St_assign(var_s, diff);

	// else
	auto sum = new Exp_operation2(Operator_PLUS, var_s, var_i);
	auto els = new St_assign(var_s, sum);

	return new St_if(cond, then, els);
}


Statement *make_while()
{
	// cond
	auto var_i = new Exp_variable("i");
	auto var_n = new Exp_variable("n");
	auto cond = new Exp_operation2(Operator_LE, var_i, var_n);

	// body
	auto st_1 = make_if();

	auto cst_1 = new Exp_constant(Type_INT, 1);
	auto exp_sum = new Exp_operation2(Operator_PLUS, var_i, cst_1);
	auto st_2 = new St_assign(var_i, exp_sum);

	auto body = new St_list( {st_1, st_2} );

	// while
	return new St_while(cond, body);
}


int main(void)
{
	auto var_s = new Exp_variable("s");
	auto st_ret = new St_return(var_s);
	st_ret->print(std::cout);

#ifdef _MSC_FULL_VER // vc++のみ止める
	std::getchar();
#endif

	return 0;
}