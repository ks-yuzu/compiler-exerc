#include <iostream>
#include "ast.h"


Statement *make_if()
{
	// cond
	auto var_i = new Exp_variable("i");
	auto cst_0 = new Exp_constant(Type_INT, 0);
	auto cond = new Exp_operation2(Operator_LT, var_i, cst_0);

	// then
	auto var_s = new Exp_variable("s");
	auto diff = new Exp_operation2(Operator_MINUS, var_s, var_i);
	auto then = new St_assign(var_s, diff);

	// else
	auto sum = new Exp_operation2(Operator_PLUS, var_s, var_i);
	auto els = new St_assign(var_s, sum);

	return new St_if(cond, then, els);
}

int main(void)
{
	auto s = make_if();
	s->print(std::cout);

#ifdef _MSC_FULL_VER // vc++のみ止める
	std::getchar();
#endif

	return 0;
}