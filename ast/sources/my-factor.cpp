﻿//*********************************************************************
//   factor.mc の ast を作るテスト用プログラム
//*********************************************************************
#include <iostream>
#include <string>
#include <list>
#include "ast.h"


// factor 中の while 文中の if 文
Statement* make_if()
{
  // n % d == 0
  auto *v_1_n = new Exp_variable("n");
  auto *v_1_d = new Exp_variable("d");
  auto *e_1_1 = new Exp_operation2(Operator_MOD, v_1_n, v_1_d);
  auto *c_1   = new Exp_constant(Type_INT, 0);
  auto *cond  = new Exp_operation2(Operator_EQ, e_1_1, c_1);

  // putint(d);
  auto *v_2 = new Exp_variable("d");
  std::list<Expression*> arglist2{v_2};
  auto *s_2 = new St_function("putint", arglist2);

  // putchar(separator);
  auto *v_3 = new Exp_variable("separator");
  std::list<Expression*> arglist3{v_3};
  auto *s_3 = new St_function("putchar",arglist3);

  // n = n / d;
  auto *v_4_1 = new Exp_variable("n");
  auto *v_4_2 = new Exp_variable("d");
  auto *e_4   = new Exp_operation2(Operator_DIV, v_4_1, v_4_2);
  auto *v_4_3 = new Exp_variable("n");
  auto *s_4   = new St_assign(v_4_3, e_4);

  // then
  std::list<Statement*> slist1{s_2, s_3, s_4};
  Statement* then = new St_list(slist1);

  // d = d + 1;
  auto *v_5_1 = new Exp_variable("d");
  auto *c_5   = new Exp_constant(Type_INT, 1);
  auto *e_5   = new Exp_operation2(Operator_PLUS, v_5_1, c_5);
  auto *v_5_2 = new Exp_variable("d");
  auto *s_5   = new St_assign(v_5_2, e_5);

  // else
  std::list<Statement*> slist2{s_5};
  auto *else_ = new St_list(slist2);

  return new St_if(cond,then,else_);
}


Statement* make_while()
{
  // d * d <= n
  auto *v_1_1 = new Exp_variable("d");
  auto *v_1_2 = new Exp_variable("d");
  auto *e_1_1 = new Exp_operation2(Operator_MUL, v_1_1, v_1_2);
  auto *v_1_3 = new Exp_variable("n");
  auto *cond  = new Exp_operation2(Operator_LE, e_1_1, v_1_3);
  Statement* body = make_if();
  return new St_while(cond,body);
}


Function* make_factor()
{
  // int n;
  auto *v_n = new Variable(Type_INT, "n");

  // int d;
  auto *v_d = new Variable(Type_INT, "d");

  // d = 2;
  auto *v_1 = new Exp_variable("d");
  auto *c_1 = new Exp_constant(Type_INT, 2);
  auto *s_1 = new St_assign(v_1, c_1);

  // while
  Statement* s_2 = make_while();

  // putint(n);
  auto *v_3 = new Exp_variable("n");
  std::list<Expression*> arglist3{v_3};
  auto *s_3 = new St_function("putint",arglist3);

  // putchar('\n');
  auto *c_4 = new Exp_constant(Type_CHAR, '\n');
  std::list<Expression*> arglist4{c_4};
  auto *s_4 = new St_function("putchar",arglist4);

  std::list<Variable*>  arg_list{v_n};
  std::list<Variable*>  lv_list {v_d};
  std::list<Statement*> st_list {s_1, s_2, s_3, s_4};
  St_list* sl = new St_list(st_list);
  return new Function(Type_INT, "factor", arg_list, lv_list, sl);
}


Function* make_main()
{
  // int n;
  auto *v_n = new Variable(Type_INT, "n");

  // putchar('n');
  auto *c_1 = new Exp_constant(Type_CHAR, 'n');
  std::list<Expression*> arglist1{c_1};
  auto *s_1 = new St_function("putchar",arglist1);

  // putchar('=');
  auto *c_2 = new Exp_constant(Type_CHAR, '=');
  std::list<Expression*> arglist2{c_2};
  auto *s_2 = new St_function("putchar",arglist2);

  // n = getint();
  Exp_variable* v_3 = new Exp_variable("n");
  std::list<Expression*> arglist3{};
  auto *f_3 = new Exp_function("getint",arglist3);
  auto *s_3 = new St_assign(v_3, f_3);

  // separator = '*';
  auto *v_4 = new Exp_variable("separator");
  auto *c_4 = new Exp_constant(Type_CHAR,'*');
  auto *s_4 = new St_assign(v_4, c_4);

  // function(n);
  auto *v_5 = new Exp_variable("n");  
  std::list<Expression*> arglist5{v_5};
  auto *s_5 = new St_function("factor",arglist5);

  std::list<Variable*> arg_list{};
  std::list<Variable*> lv_list{v_n};
  std::list<Statement*> st_list{s_1, s_2, s_3, s_4, s_5};
  auto *sl  = new St_list(st_list);
  return new Function(Type_INT, "main", arg_list, lv_list, sl);
}


int main(void)
{
  // char separator;   
  auto *v = new Variable(Type_CHAR, "separator");

  // factor
  Function *f_factor = make_factor();

  // main
  Function *f_main = make_main();

  // variable declaration
  std::list<Variable*> vlist{v};

  // function declaration
  std::list<Function*> flist{f_factor};

  Program* prog = new Program(vlist, flist, f_main);
  prog->print(std::cout);

#ifdef _MSC_FULL_VER // vc++のみ止める
	std::getchar();
#endif

  return 0;
};