﻿//=====================================================================
//
//     ast.cpp: mini-C プログラムの抽象構文木 (実装)
//
//            コンパイラ実習  2004 (c) 平岡佑介, 石浦菜岐佐
//
//=====================================================================

#include <sstream>
#include <algorithm>
#include "ast.h"

//---------------------------------------------------------------------
//   Type_string の実装
//---------------------------------------------------------------------
std::string Type_string(Type t)
{
  switch (t) {
    case Type_INT:  return "int";
    case Type_CHAR: return "char";
    default:        return "UNDEF";
  }
}

//---------------------------------------------------------------------
//   Opeartor_string の実装
//---------------------------------------------------------------------
std::string Operator_string(Operator o)
{
  switch (o) {
    case Operator_PLUS:  return "+";
    case Operator_MINUS: return "-";
    case Operator_MUL:   return "*";
    case Operator_DIV:   return "/";
    case Operator_MOD:   return "%";
    case Operator_LT:    return "<";
    case Operator_GT:    return ">";
    case Operator_LE:    return "<=";
    case Operator_GE:    return ">=";
    case Operator_NE:    return "!=";
    case Operator_EQ:    return "==";
    default:             return "???";
  }
}

//---------------------------------------------------------------------
//  tab(int)
//    インデントを行うための関数 (ast.cpp の中だけで用いる)
//    indent で指定された段数の 2 倍のスペースを返す
//---------------------------------------------------------------------
static std::string tab(int indent)
{
  std::string tab_ = "";
  for(int i=0; i<indent; i++){
    tab_ += "  ";
  }
  return tab_;
}

//---------------------------------------------------------------------
//   Exp_constant::print の実装
//---------------------------------------------------------------------
void Exp_constant::print(std::ostream& os) const
{
  switch(type()) {
    case Type_INT: os << value(); break;
    case Type_CHAR: {
      os << '\'';
      if (value()=='\n') {
        os << '\\';
        os << 'n';
      }
      else if (value()=='\t') {
        os << '\\';
        os << 't';
      }
      else if (value()=='\\') {
        os << '\\';
        os << '\\';
      }
      else {
        os << (char) value();
      }
      os << '\'';
      break;
    }
    default: assert(0);
  }
}


inline void PrintSafeCall(const Expression *exp, std::ostream& os)
{
	if( exp ) { exp->print(os); }
	else      { os << "UNDEF";  }
}


inline void PrintSafeCall(const Statement *st, std::ostream& os, int indent)
{
	if( st ) { st->print(os, indent); }
	else     { os << "UNDEF";  }
}


template<class T>
inline std::string PrintAndJoin(const std::list<T *>& data)
{
	std::stringstream buf;
	for(const auto elm : data) { elm->print(buf);  buf << ", "; }
	auto str = buf.str();
	return str.empty() ? str : std::move( str.erase(str.length() - 2) );
}



void Exp_variable::print(std::ostream& os) const
{
	os << name();
}


void Exp_operation1::print(std::ostream& os) const
{
	os << '(';
	os << Operator_string( operation() );
	PrintSafeCall(operand(), os);
	os << ')';
}


void Exp_operation2::print(std::ostream& os) const
{
	os << '(';
	PrintSafeCall(operand1(), os);
	os  << ' ' << Operator_string( operation() ) << ' ';
	PrintSafeCall(operand2(), os);
	os << ')';

}


Exp_function::~Exp_function()
{
	for(auto arg : args()) { delete arg; }
}


void Exp_function::print(std::ostream& os) const
{
	os << name() + '(' << PrintAndJoin(args()) << ')';
}


void St_assign::print(std::ostream& os, int indent) const
{
	os << tab(indent);
	PrintSafeCall(lhs(), os);
	os << " = ";
	PrintSafeCall(rhs(), os);
	os << ";" << std::endl;
}


St_list::~St_list()
{
	for(auto st : statements()){ delete st; }
}


void St_list::print(std::ostream& os, int indent) const
{
	for(const auto st : statements()) { st->print(os, indent); }
}


void St_if::print(std::ostream& os, int indent) const
{
	os << tab(indent) << "if( ";
	PrintSafeCall(condition(), os);
	os << " )" << std::endl;
	
	os << tab(indent) << "{" << std::endl;
	PrintSafeCall(then_part(), os, indent + 1);
	os << tab(indent) << "}" << std::endl;

	os << tab(indent) << "else" << std::endl;

	os << tab(indent) << "{" << std::endl;
	PrintSafeCall(else_part(), os, indent + 1);
	os << tab(indent) << "}" << std::endl;
}


void St_while::print(std::ostream& os, int indent) const
{
	os << tab(indent) << "while( ";
	PrintSafeCall(condition(), os);
	os << " )" << std::endl;
	
	os << tab(indent) << "{" << std::endl;
	PrintSafeCall(body(), os, indent + 1);
	os << tab(indent) << "}" << std::endl;
}


void St_return::print(std::ostream& os, int indent) const
{
	os << tab(indent) << "return ";
	PrintSafeCall(value(), os);
	os << ";" << std::endl;
}


void St_function::print(std::ostream& os, int indent) const
{
	os << tab(indent);
	PrintSafeCall(&function_, os);
	os << ";" << std::endl;
}


void Variable::print(std::ostream& os) const
{
	os << Type_string(type()) << " " << name_;
}


Function::~Function()
{
	for(auto arg : args()) { delete arg; }
	for(auto local_var : local_vars()) { delete local_var; }
}

void Function::print(std::ostream& os) const
{
	os << Type_string(type()) << " " << name() << "(" << PrintAndJoin(args()) << ")" << std::endl;
	os << "{" << std::endl;
	for(const auto def : local_vars())
	{
		os << tab(1);
		def->print(os);
		os << ";" << std::endl;
	}
	body()->print(os, 1);
	os << "}" << std::endl;
}


Program::~Program()
{
	for(auto var : vars()) { delete var; }
	for(auto func : funcs()) { delete func; }
	delete main();
}


void Program::print(std::ostream& os) const
{
	for(const auto var : vars())   { var->print(os);  os << ";" << std::endl; }
	for(const auto func : funcs()) { func->print(os); }
	main()->print(os);
}