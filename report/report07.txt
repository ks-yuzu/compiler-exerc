科目名: コンパイラ
課題: R7
番号: 3522
氏名: 大迫 裕樹
作成: 2015年 9月 18日

----------------------------------------
[課題 7.1]
1.何を行ったか
  字句解析 - 空白の処理

2.結果
  4: type = KW_INT     token = "int"
  4: type = KW_CHAR    token = "char"
4: 不正な文字 'e

3.考察
cygwin上で処理するには，
  flex lex.ll && \
  g++ -std=gnu++11 -g -o testlex testlex.o lex.yy.c && \
  ./testlex testlex.txt
と，-std=gnu++11の指定する必要があった（C++11）
-std=c++11では通らなかったため，注意する必要がある．
（linux環境では-std=c++11で問題なく通る）

----------------------------------------
[課題 7.2]
1.何を行ったか
  字句解析 - フロー制御のキーワードの定義

2.結果
  4: type = KW_INT     token = "int"
  4: type = KW_CHAR    token = "char"
  4: type = KW_ELSE    token = "else"
  4: type = KW_IF      token = "if"
  4: type = KW_RETURN  token = "return"
  4: type = KW_WHILE   token = "while"
5: 不正な文字 ';'

3.考察
  charやintと同様にelsやif，return等のキーワードを定義した．
  return文を1つ書くだけなので特に問題はなかった．

----------------------------------------
[課題 7.3]
1.何を行ったか
  字句解析 - 区切り記号及び演算記号の定義

2.結果
  ...
  4: type = KW_WHILE   token = "while"
  5: type = SEMICOLON  token = ";"
  5: type = COMMA      token = ","
  5: type = LPAR       token = "("
  5: type = RPAR       token = ")"
  5: type = LBRACE     token = "{"
  5: type = RBRACE     token = "}"
  5: type = LBRACK     token = "["
  5: type = RBRACK     token = "]"
  6: type = MINUS      token = "-"
  6: type = PLUS       token = "+"
  6: type = STAR       token = "*"
  6: type = SLASH      token = "/"
  6: type = PERCENT    token = "%"
  6: type = AND        token = "&"
  6: type = EQ         token = "="
  6: type = EQEQ       token = "=="
  6: type = NE         token = "!="
  6: type = GT         token = ">"
  6: type = GE         token = ">="
  6: type = LT         token = "<"
  6: type = LE         token = "<="
7: 不正な文字 '1'

3.考察
  今回も7.2と同様の形の定義を書くだけである．
  とくに注意する点もなく，演算子とその名前の対応を間違えないように
  すれば問題ないと思われる．

----------------------------------------
[課題 7.4]
1.何を行ったか
  字句解析 - 整数定数の処理

2.結果
  ...
  6: type = LE         token = "<="
  7: type = INT        token = "1"  val = 1
  7: type = INT        token = "2"  val = 2
  7: type = INT        token = "12"  val = 12
  7: type = INT        token = "123"  val = 123
  7: type = INT        token = "0123456"  val = 123456
  7: type = INT        token = "00123456789"  val = 123456789
8: 不正な文字 'a'

3.考察
  Cパートに記述した{}付きのパターンは，その定義をBパートに記述する．
  今回は整数の定義{num}であるので，
    [0-9]+
  として，0-9の任意の文字の1回以上の繰り返しとして定義している．

----------------------------------------
[課題 7.5]
1.何を行ったか
  字句解析 - 識別子の処理

2.結果
  ...
  7: type = INT        token = "00123456789"  val = 123456789
  8: type = ID         token = "a"
  8: type = ID         token = "b"
  8: type = ID         token = "c"
  8: type = ID         token = "abc"
  8: type = ID         token = "ABC"
  8: type = ID         token = "main"
  8: type = ID         token = "x123"
  8: type = ID         token = "p000Ax3"
  9: type = ID         token = "_program"
  9: type = ID         token = "u_p0x_A0x_774"
10: 不正な文字 '''

3.考察
  今回もパターンの定義とその正規表現は分割している．（Bパート，Cパート）
  C言語における識別子は，
    "アルファベットもしくはアンダースコアから始まり，それに続く英数字もしくはアンダースコアの列"
  として定義されるので，それに対応する正規表現を記述した．
  1文字の変数名は可能なので，2文字目以降は"*"指定で0文字以上としている．

----------------------------------------
[課題 7.6]
1.何を行ったか
  字句解析 - 文字定数

2.結果
  9: type = ID         token = "u_p0x_A0x_774"
 10: type = CHAR       token = "'a'"  val = 97
 10: type = CHAR       token = "'b'"  val = 98
 10: type = CHAR       token = "'0'"  val = 48
10: 不正な文字 '''

3.考察
  シングルクォートは読み飛ばすので，「'.'」でマッチさせた後に，
  クォートの内部の文字だけを取り出して利用する．
  lexの正規表現は文字列の記述をダブルクォートで囲むので気をつける必要がある.

----------------------------------------
[課題 7.7]
1.何を行ったか
  字句解析 - エスケープが必要な文字の処理

2.結果
  9: type = ID         token = "u_p0x_A0x_774"
 10: type = CHAR       token = "'a'"  val = 97
 10: type = CHAR       token = "'b'"  val = 98
 10: type = CHAR       token = "'0'"  val = 48
 10: type = CHAR       token = "'\t'"  val = 9
 10: type = CHAR       token = "'\n'"  val = 10
 10: type = CHAR       token = "'\''"  val = 39
 10: type = CHAR       token = "'\\'"  val = 92

  % ./testlex testlex.txt > tmp.txt && diff testlex_out.txt tmp.txt 
  （一致）

3.考察
  この課題で一唯ややこしいのはこの部分である．
  パターンの定義部分にもエスケープが必要なので，
    "'\\t'" 
  などとする必要があることに注意しなければならない．
  特に，'\\'にマッチングさせるにはそれぞれに更にエスケープを行い，
    "'\\\\'"
  として1文字扱いになる．
	
----------------------------------------
[課題 7 の感想]

解析したい部分の正規表現を記述していくだけであり，
その表現もテキストに記載があったので特に問題なかった．
記述の順番は少し難しい点かもしれないが，それも指示があったので，
スムーズに進むことができた．
