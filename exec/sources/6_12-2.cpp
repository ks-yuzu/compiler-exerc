﻿#include <iostream>
#include "ast.h"

Statement *make_if();
Statement *make_while();
Function *make_function_asum();

int main(void)
{
	std::list<Expression*> arglist;
	arglist.push_back(new Exp_constant(Type_INT, 5));
	Expression* exp_function_asum = new Exp_function("asum", arglist);
	std::map<std::string,Function*> func;
	std::map<std::string,int> gvar;
	std::map<std::string,int> lvar;
	func["asum"] = make_function_asum();
	int asum5 = exp_function_asum->exec(func, lvar, gvar);
	std::cout << "asum(5) = " << asum5 <<std::endl;

#ifdef _MSC_FULL_VER // vc++のみ止める
	std::getchar();
#endif

	return 0;
}


Statement *make_if()
{
	// cond
	auto var_i = new Exp_variable("i");
	auto cst_0 = new Exp_constant(Type_INT, 0);
	auto cond = new Exp_operation2(Operator_LT, var_i, cst_0);

	// then
	auto var_s = new Exp_variable("s");
	auto diff = new Exp_operation2(Operator_MINUS, var_s, var_i);
	auto then = new St_assign(var_s, diff);

	// else
	auto sum = new Exp_operation2(Operator_PLUS, var_s, var_i);
	auto els = new St_assign(var_s, sum);

	return new St_if(cond, then, els);
}


Statement *make_while()
{
	// cond
	auto var_i = new Exp_variable("i");
	auto var_n = new Exp_variable("n");
	auto cond = new Exp_operation2(Operator_LE, var_i, var_n);

	// body
	auto st_1 = make_if();
	auto cst_1 = new Exp_constant(Type_INT, 1);
	auto exp_sum = new Exp_operation2(Operator_PLUS, var_i, cst_1);
	auto st_2 = new St_assign(var_i, exp_sum);

	auto body = new St_list( {st_1, st_2} );

	// while
	return new St_while(cond, body);
}


Function *make_function_asum()
{
	// arg, localVars
	auto vardef_n = new Variable(Type_INT, "n");
	auto vardef_s = new Variable(Type_INT, "s");
	auto vardef_i = new Variable(Type_INT, "i");

	// body
	auto var_s = new Exp_variable("s");
	auto cst_0 = new Exp_constant(Type_INT, 0);
	auto var_i = new Exp_variable("i");
	auto var_n = new Exp_variable("n");
	auto minus_n = new Exp_operation1(Operator_MINUS, var_n);

	auto st_assign1 = new St_assign(var_s, cst_0);
	auto st_assign2 = new St_assign(var_i, minus_n);
	auto st_return  = new St_return(var_s);

	auto body = new St_list({st_assign1, st_assign2, make_while(), st_return});

	return new Function(Type_INT, "asum", {vardef_n}, {vardef_s, vardef_i}, body);
}
