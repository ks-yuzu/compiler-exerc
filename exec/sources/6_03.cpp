﻿#include <iostream>
#include "ast.h"


int main(void)
{
	// 5.03 そのまま
	Expression* v = new Exp_variable("n");
	Expression* o = new Exp_operation1(Operator_MINUS, v);
	// テスト
	std::map<std::string,Function*> func;
	std::map<std::string,int> gvar;
	std::map<std::string,int> lvar;
	lvar["n"] = 31;
	std::cout << o->exec(func, lvar, gvar); std::cout<<std::endl;

#ifdef _MSC_FULL_VER // vc++のみ止める
	std::getchar();
#endif

	return 0;
}