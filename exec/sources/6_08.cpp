﻿#include <iostream>
#include "ast.h"

Statement *make_if();


int main(void)
{
	Statement* s = make_if(); // 前回の演習で作成したもの
	std::map<std::string,Function*> func;
	std::map<std::string,int> gvar;
	std::map<std::string,int> lvar;
	// 真の場合
	lvar["i"] = -5;
	lvar["s"] = 10;
	Return_t rd1 = s->exec(func, lvar, gvar);
	std::cout << "s = " << lvar["s"] <<std::endl;
	// 偽の場合
	lvar["i"] = 7;
	lvar["s"] = 10;
	Return_t rd0 = s->exec(func, lvar, gvar);
	std::cout << "s = " << lvar["s"] <<std::endl;

#ifdef _MSC_FULL_VER // vc++のみ止める
	std::getchar();
#endif

	return 0;
}


Statement *make_if()
{
	// cond
	auto var_i = new Exp_variable("i");
	auto cst_0 = new Exp_constant(Type_INT, 0);
	auto cond = new Exp_operation2(Operator_LT, var_i, cst_0);

	// then
	auto var_s = new Exp_variable("s");
	auto diff = new Exp_operation2(Operator_MINUS, var_s, var_i);
	auto then = new St_assign(var_s, diff);

	// else
	auto sum = new Exp_operation2(Operator_PLUS, var_s, var_i);
	auto els = new St_assign(var_s, sum);

	return new St_if(cond, then, els);
}