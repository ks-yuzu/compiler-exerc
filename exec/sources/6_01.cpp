﻿#include <iostream>
#include "ast.h"


int main(void)
{
	// 課題5.1 と同じ抽象構文木
	Expression* c1 = new Exp_constant(Type_INT, 7);
	Expression* c2 = new Exp_constant(Type_CHAR, 'x');

	// 実行とその結果の表示
	std::map<std::string,Function*> func;
	std::map<std::string,int> gvar;
	std::map<std::string,int> lvar;
	std::cout << c1->exec(func, lvar, gvar); std::cout<<std::endl;
	std::cout << c2->exec(func, lvar, gvar); std::cout<<std::endl;

#ifdef _MSC_FULL_VER // vc++のみ止める
	std::getchar();
#endif

	return 0;
}