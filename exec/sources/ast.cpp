﻿//=====================================================================
//
//     ast.cpp: mini-C プログラムの抽象構文木 (実装)
//
//            コンパイラ実習  2004 (c) 平岡佑介, 石浦菜岐佐
//
//=====================================================================

#include <sstream>
#include <cstdlib>
#include "ast.h"


// using alias
template <typename T> using RemoveConst = typename std::remove_const<T>::type;
template <typename T> using RemoveRef = typename std::remove_reference<T>::type;
template <typename T> using OnlyType = typename ::RemoveConst<RemoveRef<T>>;


//---------------------------------------------------------------------
//   Type_string の実装
//---------------------------------------------------------------------
std::string Type_string(Type t)
{
  switch (t) {
    case Type_INT:  return "int";
    case Type_CHAR: return "char";
    default:        return "UNDEF";
  }
}

//---------------------------------------------------------------------
//   Opeartor_string の実装
//---------------------------------------------------------------------
std::string Operator_string(Operator o)
{
  switch (o) {
    case Operator_PLUS:  return "+";
    case Operator_MINUS: return "-";
    case Operator_MUL:   return "*";
    case Operator_DIV:   return "/";
    case Operator_MOD:   return "%";
    case Operator_LT:    return "<";
    case Operator_GT:    return ">";
    case Operator_LE:    return "<=";
    case Operator_GE:    return ">=";
    case Operator_NE:    return "!=";
    case Operator_EQ:    return "==";
    default:             return "???";
  }
}

//---------------------------------------------------------------------
//  tab(int)
//    インデントを行うための関数 (ast.cpp の中だけで用いる)
//    indent で指定された段数の 2 倍のスペースを返す
//---------------------------------------------------------------------
static std::string tab(int indent)
{
  std::string tab_ = "";
  for(int i=0; i<indent; i++){
    tab_ += "  ";
  }
  return tab_;
}

//---------------------------------------------------------------------
//   Exp_constant::print の実装
//---------------------------------------------------------------------
void Exp_constant::print(std::ostream& os) const
{
  switch(type()) {
    case Type_INT: os << value(); break;
    case Type_CHAR: {
      os << '\'';
      if (value()=='\n') {
        os << '\\';
        os << 'n';
      }
      else if (value()=='\t') {
        os << '\\';
        os << 't';
      }
      else if (value()=='\\') {
        os << '\\';
        os << '\\';
      }
      else {
        os << (char) value();
      }
      os << '\'';
      break;
    }
    default: assert(0);
  }
}


inline void PrintSafeCall(const Expression *exp, std::ostream& os)
{
	if( exp ) { exp->print(os); }
	else      { os << "UNDEF";  }
}


inline void PrintSafeCall(const Statement *st, std::ostream& os, int indent)
{
	if( st ) { st->print(os, indent); }
	else     { os << "UNDEF";  }
}


template <class T>
std::list<std::string> GetPrintMesList(const std::list<T *>& data)
{
	std::list<std::string> mesList;

	for(const auto m : data)
	{
		std::ostringstream buf;
		m->print(buf);
		mesList.push_back( buf.str() );
	}

	return std::move(mesList);
}


inline std::string Join(const std::list<std::string>& strs, const std::string& sep = ", ")
{
	std::string buf("");
	for(const auto m : strs) { buf = buf + m + sep; }
	return buf.empty() ? buf : std::move( buf.erase(buf.length() - sep.length()) );
}


[[noreturn]] void ErrorExit(std::string mes)
{
	std::cout << mes << std::endl;
	exit(1);
}


inline void NullCheck(const void *p)
{
	if( !p ) { ErrorExit("null pointer exception"); }
}


void Exp_variable::print(std::ostream& os) const
{
	os << name();
}


int Exp_variable::exec(
	const std::map<std::string, Function *>& func,
	const std::map<std::string, int>& lvar,
	const std::map<std::string, int>& gvar
) const
{
	const auto lRes = lvar.find(name());
	if( lRes != std::end(lvar) ){ return lRes->second; }

	const auto gRes = gvar.find(name());
	if( gRes != std::end(gvar) ){ return gRes->second; }

	ErrorExit(name() + " is not declared");
}


void Exp_operation1::print(std::ostream& os) const
{
	os << '(';
	os << Operator_string( operation() );
	PrintSafeCall(operand(), os);
	os << ')';
}


int Exp_operation1::exec(
	const std::map<std::string, Function *>& func,
	const std::map<std::string, int>& lvar,
	const std::map<std::string, int>& gvar
) const
{
	NullCheck( operand() );

	switch( operation() )
	{
		case Operator_PLUS:   return operand()->exec(func, lvar, gvar);
		case Operator_MINUS:  return -( operand()->exec(func, lvar, gvar) );
		default:              ErrorExit("\"" + Operator_string(operation()) + "\" needs 2 operands");
	}
}


void Exp_operation2::print(std::ostream& os) const
{
	os << '(';
	PrintSafeCall(operand1(), os);
	os  << ' ' << Operator_string( operation() ) << ' ';
	PrintSafeCall(operand2(), os);
	os << ')';
}


int Exp_operation2::exec(
	const std::map<std::string, Function *>& func,
	const std::map<std::string, int>& lvar,
	const std::map<std::string, int>& gvar
) const
{
	NullCheck( operand1() );
	NullCheck( operand2() );

	int lop = operand1()->exec(func, lvar, gvar); //left oprand
	int rop = operand2()->exec(func, lvar, gvar); //right oprand

	switch( operation() )
	{
		case Operator_PLUS:   return lop + rop;
		case Operator_MINUS:  return lop - rop;
		case Operator_MUL:    return lop * rop;
		case Operator_DIV:    return lop / rop;
		case Operator_MOD:    return lop % rop;

		case Operator_LT:     return lop < rop;
		case Operator_GT:     return lop > rop;
		case Operator_LE:     return lop <= rop;
		case Operator_GE:     return lop >= rop;
		case Operator_NE:     return lop != rop;
		case Operator_EQ:     return lop == rop;

		default:              ErrorExit("internal error");
	}
}


Exp_function::~Exp_function()
{
	for(auto *arg : args()) { delete arg; }
}


void Exp_function::print(std::ostream& os) const
{
	os << name() + '(' << Join( GetPrintMesList(args()) ) << ')';
}


int Exp_function::exec(
	const std::map<std::string, Function *>& func,
	const std::map<std::string, int>& lvar,
	const std::map<std::string, int>& gvar
) const
{
	std::list<int> i_args;
	for(auto arg : args()) { i_args.push_back( arg->exec(func, lvar, gvar) ); }

	if(name() == "getint") { int tmp;  std::cin >> tmp;  return tmp; }
	else if(name() == "getchar") { char tmp;  std::cin >> tmp;  return tmp; }
	else if(name() == "putint" ) { std::cout << i_args.front();  return 0; }
	else if(name() == "putchar") { std::cout << static_cast<char>(i_args.front());  return 0; }
	else
	{
		const auto res = func.find(name()); // 関数の登録チェック
		if( res == std::end(func) ) { ErrorExit(name() + " is not declared"); }

		return res->second->exec(func, gvar, i_args);
	}
}


void St_assign::print(std::ostream& os, int indent) const
{
	os << tab(indent);
	PrintSafeCall(lhs(), os);
	os << " = ";
	PrintSafeCall(rhs(), os);
	os << ";" << std::endl;
}


Return_t St_assign::exec(
	const std::map<std::string, Function *>& func,
	const std::map<std::string, int>& lvar,
	const std::map<std::string, int>& gvar
) const
{
	assert( lhs() );  assert( rhs() );

	int i_rhs = rhs()->exec(func, lvar, gvar);

	auto lRes = const_cast<OnlyType<decltype(lvar)>&>(lvar).find( lhs()->name() );
	auto gRes = const_cast<OnlyType<decltype(gvar)>&>(gvar).find( lhs()->name() );

	if( lRes != std::end(lvar) )	  { lRes->second = i_rhs; }
	else if( gRes != std::end(gvar) ) { gRes->second = i_rhs; }
	else                              { ErrorExit(lhs()->name() + " is not declared"); }

	return Return_t(false, 0);
}


St_list::~St_list()
{
	for(auto st : statements()){ delete st; }
}


void St_list::print(std::ostream& os, int indent) const
{
	for(const auto st : statements()) { st->print(os, indent); }
}


Return_t St_list::exec(
	const std::map<std::string, Function *>& func,
	const std::map<std::string, int>& lvar,
	const std::map<std::string, int>& gvar
) const
{
	for(const auto st : statements())
	{
		assert(st);
		Return_t rd = st->exec(func, lvar, gvar);
		if( rd.val_is_returned ) { return rd; }
	}

	return Return_t(false, 0);
}


void St_if::print(std::ostream& os, int indent) const
{
	os << tab(indent) << "if( ";
	PrintSafeCall(condition(), os);
	os << " )" << std::endl;
	
	os << tab(indent) << "{" << std::endl;
	PrintSafeCall(then_part(), os, indent + 1);
	os << tab(indent) << "}" << std::endl;

	if( !else_part() ) return;

	os << tab(indent) << "else" << std::endl;

	os << tab(indent) << "{" << std::endl;
	PrintSafeCall(else_part(), os, indent + 1);
	os << tab(indent) << "}" << std::endl;
}



Return_t St_if::exec(
	const std::map<std::string, Function *>& func,
	const std::map<std::string, int>& lvar,
	const std::map<std::string, int>& gvar
) const
{
	assert( condition() );  assert( then_part() );
	if( condition()->exec(func, lvar, gvar) ) { return then_part()->exec(func, lvar, gvar); }
	else if( else_part() )                    { return else_part()->exec(func, lvar, gvar); }

	return Return_t(false, 0);
}


void St_while::print(std::ostream& os, int indent) const
{
	os << tab(indent) << "while( ";
	PrintSafeCall(condition(), os);
	os << " )" << std::endl;
	
	os << tab(indent) << "{" << std::endl;
	PrintSafeCall(body(), os, indent + 1);
	os << tab(indent) << "}" << std::endl;
}


Return_t St_while::exec(
	const std::map<std::string, Function *>& func,
	const std::map<std::string, int>& lvar,
	const std::map<std::string, int>& gvar
) const
{
	assert( condition() );  assert( body() );
	while( condition()->exec(func, lvar, gvar) )
	{
		auto ret = body()->exec(func, lvar, gvar);
		if( ret.val_is_returned ) { return ret; }
	}
	
	return Return_t(false, 0);
}


void St_return::print(std::ostream& os, int indent) const
{
	os << tab(indent) << "return ";
	PrintSafeCall(value(), os);
	os << ";" << std::endl;
}


Return_t St_return::exec(
	const std::map<std::string, Function *>& func,
	const std::map<std::string, int>& lvar,
	const std::map<std::string, int>& gvar
) const
{
	assert( value() );
	int rv = value()->exec(func, lvar, gvar);
	return Return_t(true, rv);
}


void St_function::print(std::ostream& os, int indent) const
{
	os << tab(indent);
	PrintSafeCall(&function_, os);
	os << ";" << std::endl;
}


Return_t St_function::exec(
	const std::map<std::string, Function *>& func,
	const std::map<std::string, int>& lvar,
	const std::map<std::string, int>& gvar
) const
{
	function_.exec(func, lvar, gvar);
	return Return_t(false, 0);
}


void Variable::print(std::ostream& os) const
{
	os << Type_string(type()) << " " << name_;
}


Function::~Function()
{
	for(auto arg : args()) { delete arg; }
	for(auto local_var : local_vars()) { delete local_var; }
}

void Function::print(std::ostream& os) const
{
	os << Type_string(type()) << " " << name() << "(" << Join( GetPrintMesList(args()) ) << ")" << std::endl;
	os << "{" << std::endl;
	for(const auto def : local_vars())
	{
		os << tab(1);
		def->print(os);
		os << ";" << std::endl;
	}
	body()->print(os, 1);
	os << "}" << std::endl;
}


int Function::exec(
	const std::map<std::string, Function *>& func,
	const std::map<std::string, int>& gvar,
	const std::list<int>& i_args
) const
{
	std::map<std::string, int> lvar;

	auto arg = std::begin(args());
	auto argVal = std::begin(i_args);
	for(; arg != std::end(args()); arg++, argVal++) { lvar[(*arg)->name()] = *argVal; }

	for(const auto lv : local_vars()){ lvar[lv->name()] = 0; }

	Return_t rd = body()->exec(func, lvar, gvar);

	return rd.return_val;
}


Program::~Program()
{
	for(auto var : vars())   { delete var; }
	for(auto func : funcs()) { delete func; }
	delete main();
}


void Program::print(std::ostream& os) const
{
	for(const auto var : vars())   { var->print(os);  os << ";" << std::endl; }
	for(const auto func : funcs()) { func->print(os); }
	main()->print(os);
}


int Program::exec() const
{
	std::map<std::string, int> gvar;
	for(const auto var : vars()){ gvar[var->name()] = 0; }

	std::map<std::string, Function *> funcMap;
	for(const auto func : funcs()){ funcMap[func->name()] = func; }

	return main()->exec(funcMap, gvar, std::list<int>());
}
